###### A Webpage Crawler for Different Indic Fonts

## Requirement
- Scrapy 1.5.1

## How to run
- Open Terminal
- Go inside the project folder having scrapy.cfg
- Input the following command in the terminal

    ``scrapy crawl AFCrawler -a domain=<URL_of_website>``

## Output
- Output will be stored in ``pages.json`` file
