# -*- coding: utf-8 -*-
from scrapy.exporters import JsonLinesItemExporter

class AllfontcrawlerPipeline(object):
    def open_spider(self, spider):
        self.pages = []       #used to store pages in a list


    #Function to write the data in a file named pages.json
    def close_spider(self, spider):
        pages_dict = {"pages": self.pages}    #page list is stored in a dictionary
        filepointer = open('pages.json', 'wb')
        exporter = JsonLinesItemExporter(filepointer, encoding='utf-8')
        exporter.start_exporting()
        exporter.export_item(pages_dict)
        exporter.finish_exporting()
        exporter.file.close()

    def process_item(self, item, spider):
        self.pages.append(item)
        return item
