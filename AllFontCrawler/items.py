# -*- coding: utf-8 -*-
import scrapy

class AllfontcrawlerItem(scrapy.Item):
    page = scrapy.Field()           #used to store page url
    content = scrapy.Field()        #used to store page content
