# -*- coding: utf-8 -*-
from time import sleep
import random
from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor
from AllFontCrawler.items import AllfontcrawlerItem


class AfcrawlerSpider(CrawlSpider):
    name = "AFCrawler"
    
    #Logging File Setting
    custom_settings = {
        'LOG_FILE': 'allfontcrawler.log',
    }

    #Constructor to take the url of site from terminal
    def __init__(self, domain='', *args, **kwargs):
        super(AfcrawlerSpider, self).__init__(*args, **kwargs)
        url = domain.split(':')
        if url[1][-1] == '/':
            url[1] = url[1][:-1]
        self.allowed_domains = [url[1][2:]]      #used to set allowed domain
        self.start_urls = [domain]

    rules = (Rule(LinkExtractor(), callback='parse_page', follow=True),)       #Extracting the links


    #Callback function to scrape page and content values
    def parse_page(self, response):
        #Uncomment for random scrape time for safety 
        #sleep(random.randrange(1,3))
        self.logger.info('Webpage parsed is %s', response.url) #logging info
        item = AllfontcrawlerItem()
        item['page'] = response.url
        item['content'] = response.xpath('//p/text()').extract()
        item['content'] = item['content'] if type(item['content']) is not list else ' '.join(item['content'])     #Converts list into string
        item['content'] = " ".join(item['content'].split())                          #Splits the string from \n, \t, " " and join them as a string
        yield item
